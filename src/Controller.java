import java.util.Scanner;

public class Controller {
    private Model model;
    private View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public void processUser() {
        Scanner sc = new Scanner(System.in);
        boolean gameRun = true;
        while (gameRun) {
            int userPick = inputIntValueWithScanner(sc);
            if (checkAnswer(userPick)) {
                view.printMessage(View.WRONG_DIAPASON);
            } else {
                model.addPick(userPick);
                model.setLastPick(userPick);
                view.printMessage(View.LAST_PICKS, model.getPicks());

                if (model.getSecretNumber() == model.getLastPick()) {
                    view.printMessage(View.WINNER);
                    gameRun = false;
                }
                if (model.getLastPick() > model.getSecretNumber()) {
                    model.setTo(userPick);
                    view.printMessage(userPick, View.LESSER_NUMBER);
                }
                if (model.getLastPick() < model.getSecretNumber()) {
                    model.setFrom(userPick);
                    view.printMessage(userPick, View.BIGGER_NUMBER);
                }
            }
        }
    }

    public int inputIntValueWithScanner(Scanner sc) {
        view.printMessage(model.getFrom(), model.getTo());
        while (!sc.hasNextInt()) {
            view.printMessage(View.WRONG_INPUT_INT_DATA);
            sc.next();
        }
        return sc.nextInt();
    }

    private boolean checkAnswer(int answer) {
        return answer >= model.getTo() || answer <= model.getFrom();
    }
}
