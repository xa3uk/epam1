import java.util.ArrayList;
import java.util.List;

public class Model {
    private List<Integer> picks = new ArrayList<>();
    private int lastPick;
    private int from = 0;
    private int to = 100;
    private int secretNumber = (int) (Math.random() * 100);


    public void addPick(int value) {
        picks.add(value);
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getSecretNumber() {
        return secretNumber;
    }

    public int getLastPick() {
        return lastPick;
    }

    public void setLastPick(int lastPick) {
        this.lastPick = lastPick;
    }

    public List<Integer> getPicks() {
        return picks;
    }
}
