import java.util.List;

public class View {
    public static final String INPUT_INT_DATA = "\nWhat is your next try from ";
    public static final String WRONG_INPUT_INT_DATA = "Wrong input! Repeat please! ";
    public static final String WRONG_DIAPASON = "Wrong diapason!";
    public static final String BIGGER_NUMBER = " is wrong pick. We find bigger number";
    public static final String LESSER_NUMBER = " is wrong pick. We find less number";
    public static final String WINNER = "You are winner!";
    public static final String LAST_PICKS = "Your last picks: ";

    public void printMessage(int from, int to){
        System.out.println(INPUT_INT_DATA + from + " to " + to + "?");
    }

    public void printMessage(String message){
        System.out.println(message);
    }

    public void printMessage(String message, List<Integer> values){
        System.out.println(message + values);
    }

    public void printMessage(int number, String message){
        System.out.println(number + message);
    }

}
